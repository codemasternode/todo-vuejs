import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {
      name: "Marcin",
      lastname: "Warzybok"
    },
    count: 0,
    categories: ["Computer", "Smartphone", "TV"]
  },
  mutations: {
    INCREMENT(state, value) {
      state.count += value;
    },
    ADD_TO_CATEGORY(state, data) {
      state.categories.push(data);
    }
  },
  actions: {
    call: (context, todo) => {
      console.log(todo);
      Axios.post("http://localhost:3000/todos", {
        todo
      }).then(() => {
        context.commit("ADD_TO_CATEGORY", todo);
      });
    }
  },
  getters: {
    categoriesLength: state => {
      return state.categories.length + 1;
    }
  }
});
