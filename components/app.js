Vue.component("greeting", {
  template: `
    <div>
        <p>Hey there, I am a re-usable component named {{name}}</p>
        <button @click="changeName">Zmień imię</button>
    </div>
  `,
  data: function () {
    return {
      name: "Marcin",
    };
  },
  methods: {
    changeName: function () {
      this.name = "XD";
    },
  },
});

new Vue({
  el: "#app",
});
