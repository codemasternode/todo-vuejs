new Vue({
  el: "#app",
  data: {
    surname: "Warzybok",
    website: "http://google.com",
    websiteTag: '<a href="https://wikipedia.org">Link<a>',
  },
  methods: {
    greeting(name) {
      return `Hello ${name} ${this.surname}`;
    },
  },
});
