new Vue({
  el: "#app",
  data: {
    age: 18,
    x: 0,
    y: 0,
  },
  methods: {
    operate(value) {
      this.age += value;
    },
    mousemove(e) {
      this.x = e.offsetX;
      this.y = e.offsetY;
    },
  },
});
